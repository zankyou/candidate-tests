# Welcome
This test consists in two parts and a bonus point. We will evaluate the markup and the fact that it actually works in different screen resolutions.


 ### Rules  
 - You have 2 hours to complete part 1 and 2  
 - For the bonus point you have half a hour more  
 - Create a single page for all exercises
 - Adhere to the [BEM methodology](https://en.bem.info/method/definitions/)  
 - Crossbrowser compatibility
 - Make it valid code (by W3C)
 - Any good practice applied to the structure of the project and the way the code is written will be valued positively.   
 - Do it with love


### Delivery
You must create a repository on any platform that uses Git (Bitbucket, Github...) and send us the url through the form 'Prueba Técnica Front Zankyou II'

## Part 1

Try to replicate the styles of the design  
- This module has a "background" image, a title, a subtitle and some form elements flying over.  
- Try to use the most suitable (and modern) css properties.
- The text has a dynamic shadow in the background to improve readbility  
- In the mobile version you could be creative according to UX best practise.

### Screenshots
Desktop
![part1](designs/part-1--desktop.png)

Mobile
- Up to you!

## Part 2

Get form values:  
- Show in console input values when you click on 'Mostrar' button  
- If input are empty show an alert with a description text error.


## Bonus point

A modal window to contact our providers. It consists in a header section and a form. In desktop use two columns for the form and full width in mobile.

### Definition

Create a button that opens a modal window like the design.  
 - Use [magnific popup](http://dimsemenov.com/plugins/magnific-popup/)  
 - Feel free about button design and position.

### Screenshots
Desktop  
![part3](designs/part-3--desktop.jpg)


Mobile  
![part3](designs/part-3--mobile.jpg)

---

If you have any doubts or wanna ask something you can email us anytime.

Have fun!

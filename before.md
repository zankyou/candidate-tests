We require you to use the BEM metodology. Also for some part of the test you'll have to use the Magnific popup plugin and jQuery.

Recomended links:
- [BEM metodology](https://en.bem.info/method/definitions/)
- [Magnific popup](http://dimsemenov.com/plugins/magnific-popup/)
